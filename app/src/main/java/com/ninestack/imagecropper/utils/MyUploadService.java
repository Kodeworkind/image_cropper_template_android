package com.ninestack.imagecropper.utils;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.ninestack.imagecropper.MainActivity;
import com.ninestack.imagecropper.R;
import com.ninestack.imagecropper.retrofit.ApiInterface;
import com.ninestack.imagecropper.retrofit.RetrofitApiUtils;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static com.ninestack.imagecropper.utils.Utils.getRealPathFromUri;

/**
 * Created by Adrian Almeida.
 */
public class MyUploadService extends MyBaseTaskService {

    /**
     * Intent Action
     */
    public static final String ACTION_UPLOAD = "action_upload";
    public static final String DELETE_FILE = "action_delete";
    public static final String ACTION_DOC_UPLOAD = "action_doc_upload";

    public static final String UPLOAD_COMPLETED = "upload_completed";
    public static final String UPLOAD_ERROR = "upload_error";

    public static final String DOC_UPLOAD_COMPLETED = "doc_upload_completed";
    public static final String DOC_UPLOAD_ERROR = "doc_upload_error";


    /**
     * Intent Extras
     **/
    public static final String EXTRA_FILE_URI = "extra_file_uri";
    public static final String EXTRA_FILE_NAME = "extra_file_name";
    public static final String EXTRA_STORAGE_PATH = "extra_storage_path";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";
    public static final String EXTRA_DOCUMENT_URI = "extra_document_uri";

    private ApiInterface apiInterface;
    Disposable imageUploadDisposable;

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPLOAD_COMPLETED);
        filter.addAction(UPLOAD_ERROR);
        filter.addAction(DOC_UPLOAD_COMPLETED);
        filter.addAction(DOC_UPLOAD_ERROR);
        return filter;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_UPLOAD.equals(intent.getAction())) {
            Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
            uploadFromUri(fileUri);
        }

        return START_REDELIVER_INTENT;

    }


    private void uploadFromUri(final Uri fileUri) {

        String filePath = getRealPathFromUri(fileUri,this);
        File file=new File(filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        String descriptionString = "Sample description";
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);

        showProgressNotification(getString(R.string.progress_uploading),
             0,0);

        //upload file to server
        if (apiInterface == null) {
            apiInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<Response<Void>> request = (Observable<Response<Void>>) RetrofitApiUtils.getPreparedObservable(
                apiInterface.uploadImageFile(body,description),MyUploadService.class,false,false);
        request.subscribe(new Observer<Response<Void>>() {
            @Override
            public void onSubscribe(@io.reactivex.annotations.NonNull Disposable disposable) {
                imageUploadDisposable = disposable;
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                showUploadFinishedNotification(false, fileUri);
                Toast.makeText(MyUploadService.this,"Failure",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNext(Response<Void> value) {
                showUploadFinishedNotification(true, fileUri);
                broadcastUploadFinished(fileUri);
                Toast.makeText(MyUploadService.this,"Success",Toast.LENGTH_SHORT).show();
                /*   if (value.isSuccessful()){

                }*/

            }
        });
    }


    /**
     * Broadcast finished upload (success or failure).
     *
     * @return true if a running receiver received the broadcast.
     */
    private boolean broadcastUploadFinished(@Nullable Uri fileUri) {

        Intent broadcast = new Intent("success")
                .putExtra(EXTRA_FILE_URI, fileUri);
        return LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);
    }

    /**
     * Show a notification for a finished upload.
     */
    private void showUploadFinishedNotification(@Nullable boolean isSuccess, @Nullable Uri fileUri) {
        // Hide the progress notification
        dismissProgressNotification();
        // Make Intent to MainActivity
        Intent intent = new Intent(this, MainActivity.class)
                .putExtra(EXTRA_FILE_URI, fileUri)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        String caption = isSuccess ? getString(R.string.upload_success) : getString(R.string.upload_failure);
        showFinishedNotification(caption,intent , isSuccess, false);
    }


}
