package com.ninestack.imagecropper.retrofit;

import java.util.ArrayList;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Adrian Almeida.
 */
public interface ApiInterface {
    @Multipart
    @POST("retrofit_example/upload_image.php")
    Observable<Response<Void>> uploadImageFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);
}
