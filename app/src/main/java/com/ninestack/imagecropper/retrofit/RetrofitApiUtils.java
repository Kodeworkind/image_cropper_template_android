package com.ninestack.imagecropper.retrofit;

import android.util.LruCache;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by Adrian Almeida.
 */
public class RetrofitApiUtils {

    private static LruCache<Class<?>, Observable<?>> apiObservables = new LruCache<>(10);

    public static RequestBody JSONToRetrofitJSON(String JSONRequest) throws JSONException {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(JSONRequest)).toString());
    }


    public static ApiInterface getGeneralAPIService() {
        return ApiClient.getClient(0).create(ApiInterface.class);
    }

    public static Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache) {
        Observable<?> preparedObservable = null;

        if (useCache) //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);

        if (preparedObservable != null)
            return preparedObservable;

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }

        return preparedObservable;
    }
}
