package com.ninestack.imagecropper.retrofit;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.ninestack.imagecropper.BuildConfig;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Adrian Almeida.
 */
public class ApiClient {


    private static Retrofit retrofit = null;

    private static Gson gson;

    private static Gson initGson(int deserializerID) {
        switch (deserializerID) {
            case 0:
                /**Not to be used*/
                gson = new Gson();
            case 1:
                //gson = new GsonBuilder().registerTypeAdapter(FeedData.class, new UserEventJsonDeserializer()).create();
                break;
            case 2:
                break;

        }
        return gson;
    }


    public static Retrofit getClient(int deserializer) {
        if (retrofit == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS);
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                httpClient.addInterceptor(logging);
            }
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(initGson(deserializer)))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

}
