package com.ninestack.imagecropper;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Vaibhav Barad on 5/2/2018.
 */
public class ImagePickerDialog extends DialogFragment {

    int style = DialogFragment.STYLE_NO_TITLE;
    int theme = R.style.CustomDialog;
    @BindView(R.id.camera)
    TextView camera;
    @BindView(R.id.gallery)
    TextView gallery;
    @BindView(R.id.cancel)
    TextView cancel;
    Unbinder unbinder;

    public static DialogFragment newInstance() {
        ImagePickerDialog fragment = new ImagePickerDialog();
        return fragment;
    }


    public interface ImagePickerDialogListener {
        void onCameraClick();

        void onGalleryClick();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(style, theme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        View view = inflater.inflate(R.layout.image_picker_dialog, container, false);
        getDialog().setCanceledOnTouchOutside(true);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.camera)
    public void onCameraClicked() {
        closeDialog();
        ImagePickerDialogListener listener = (ImagePickerDialogListener) getTargetFragment();
        listener.onCameraClick();
        dismiss();
    }

    @OnClick(R.id.gallery)
    public void onGalleryClicked() {
        closeDialog();
        ImagePickerDialogListener listener = (ImagePickerDialogListener) getTargetFragment();
        listener.onGalleryClick();
        dismiss();
    }

    @OnClick(R.id.cancel)
    public void onCancelClicked() {
        closeDialog();
    }

    public void closeDialog() {
        if (getDialog().isShowing()) {
            getDialog().dismiss();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
