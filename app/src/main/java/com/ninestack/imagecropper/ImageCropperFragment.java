package com.ninestack.imagecropper;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.ninestack.imagecropper.utils.GlideApp;
import com.ninestack.imagecropper.utils.MyUploadService;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.ninestack.imagecropper.utils.Utils.hasPermissions;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageCropperFragment extends Fragment implements  ImagePickerDialog.ImagePickerDialogListener{


    @BindView(R.id.iv_profile_pic)
    ImageView iv_profile_pic;

    ImagePicker imagePicker;
    ImagePickerCallback mImagePickerCallback;
    CameraImagePicker cameraPicker;
    private BroadcastReceiver mBroadcastReceiver;
    private String outputPath;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    private String TAG=ImageCropperFragment.class.getSimpleName();
    private Uri mDownloadUrl=null,mFileUri=null;
    public ImageCropperFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_image_cropper, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mImagePickerCallback = new ImagePickerCallback() {
            @Override
            public void onImagesChosen(List<ChosenImage> list) {
                    if (list != null && list.size() > 0) {
                        String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                        String cacheDirectory = getActivity().getFilesDir().toString();
                        File file = new File(cacheDirectory, fileName + ".png");
                        if (file.exists()) {
                            file.delete();
                        }
                        Uri destination = Uri.fromFile(file);
                        Log.i(TAG,""+destination.toString());
                        Crop.of(Uri.parse(list.get(0).getQueryUri()), destination).asSquare().start(getActivity(), ImageCropperFragment.this);
                    }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        };

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("Image Broadcast", "onReceive:" + intent);
                //hideProgressDialog();
                switch (intent.getAction()) {
                    case MyUploadService.UPLOAD_COMPLETED:
                        onUploadResultIntent(intent);
                        break;
                }
            }
        }

        ;
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.registerReceiver(mBroadcastReceiver, MyUploadService.getIntentFilter());
    }

    private void onUploadResultIntent(Intent intent) {
        mDownloadUrl = intent.getParcelableExtra(MyUploadService.EXTRA_DOWNLOAD_URL);
        mFileUri = intent.getParcelableExtra(MyUploadService.EXTRA_FILE_URI);
        Toast.makeText(getActivity(),"SUccess",Toast.LENGTH_SHORT).show();
        //Api call to presenter
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment newFragment = ImagePickerDialog.newInstance();
                newFragment.setTargetFragment(ImageCropperFragment.this, 300);
                newFragment.show(ft, "dialog");
            }
        }
    }


      @OnClick(R.id.iv_profile_pic)
    void profilePicUpload() {
          if (!hasPermissions(getActivity(), PERMISSIONS)) {
              requestPermissions(PERMISSIONS, PERMISSION_ALL);
          } else {
              FragmentTransaction ft = getFragmentManager().beginTransaction();
              Fragment prev = getFragmentManager().findFragmentByTag("dialog");

              if (prev != null) {
                  ft.remove(prev);
              }
              ft.addToBackStack(null);
              DialogFragment newFragment = ImagePickerDialog.newInstance();
              newFragment.setTargetFragment(ImageCropperFragment.this, 300);
              newFragment.show(ft, "dialog");
          }
    }

    @Override
    public void onCameraClick() {
        cameraPicker = new CameraImagePicker(ImageCropperFragment.this);
        cameraPicker.setImagePickerCallback(mImagePickerCallback);
        outputPath = cameraPicker.pickImage();
    }

    @Override
    public void onGalleryClick() {
        imagePicker = new ImagePicker(ImageCropperFragment.this);
        imagePicker.setImagePickerCallback(mImagePickerCallback);
        imagePicker.pickImage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK){
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(ImageCropperFragment.this);
                    imagePicker.setImagePickerCallback(mImagePickerCallback);
                }
                imagePicker.submit(data);
                return;
            }

            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(ImageCropperFragment.this);
                    cameraPicker.reinitialize(outputPath);
                    cameraPicker.setImagePickerCallback(mImagePickerCallback);
                }
                cameraPicker.submit(data);
                return;
            }

            if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(data);
            }
        }


    }

    /**
     *
     * @param data :cropped image data
     */
    private void handleCrop(Intent data) {
       // Crop.getOutput(data); //path
        Log.i(TAG,"File path  : "+Crop.getOutput(data).toString());
        GlideApp.with(ImageCropperFragment.this).load(Crop.getOutput(data)).into(iv_profile_pic);

        //start service
        getActivity().startService(new Intent(getActivity(), MyUploadService.class)
                .putExtra(MyUploadService.EXTRA_FILE_URI, Crop.getOutput(data))
                .setAction(MyUploadService.ACTION_UPLOAD));

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("picker_path", outputPath);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                outputPath = savedInstanceState.getString("picker_path");
            }
        }
    }

}
